IOC容器:控制反转
IOC容器，负责自动存储管理接口的实现类，这些对象被称为Bean
对需要管理的类进行注解
最原始的方法是使用@Component注解，但是为了专精某个模块，使用不同的注解，都包含了@Component注解
1.对mapper层，使用@Mapper
2.对service层，使用@Service
3.对controller层，使用@RestController

需要注意的是，spring容器只会对@Component相关注解的类创建【一个】对象


DI:依赖注入
当接口需要注入对应的对象时，使用对应的注解如@Resource 、@Qualifier 等

方式1. 两者配合使用，但是Qualifier可以设置的属性较少
核心：先类型后value
@Autowired
@Qualifier(value = "adminservice") //指定注入bean名称

方式2. Resource可以设置的属性多，用起来功能更完善
核心：先name后类型
@Resource(name="adminservice") //指定注入bean名称

mapper  —>  service —>  controller
都要使用依赖注入