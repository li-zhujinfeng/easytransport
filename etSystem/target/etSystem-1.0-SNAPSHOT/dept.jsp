﻿
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--加入jstl 标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%--某些模板导入--%>
<head>
<meta charset="utf-8"/>
<meta name="author" content="order by dede58.com"/>
<title>部门信息</title>
<meta name="author" content="DeathGhost" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->
 <%--jsp = html+servlet--%>
<%-- 注意顺序，先bootstrap.min.css
                   再到jquery
                   再到bootstrap.min.js
 否则某些组件不能正常显示--%>
 <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
 <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"
       integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
 <script src="/js/jquery-3.3.1.js"></script>
 <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
 <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"
         integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
         crossorigin="anonymous"></script>
<%-- <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">--%>
<%-- <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

 <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
 <script src="js/verificationNumbers.js"></script>
 <script src="js/Particleground.js"></script>
 <link rel="stylesheet" href="./css/fileinput.min.css">
 <script src="./js/fileinput.min.js"></script>
 <style>
  dd{
   font-size: 15px;
   font-family: 微軟正黑體;
   text-align: center;
  }
  dt{
   background-color:lightgreen;
   font-family: 黑体;
   font-size: 20px;
   text-align: center;
  }
  th{
   background-color: #c1e2b3;
   text-align: center;
   font-family: 楷体;
   font-size: 19px;
   /*font-weight: bold;*/
  }
  td{
   text-align: center;
   font-family: 等线;
   font-size: 15px;
  }
 </style>
</head>
<body>


<%--================以下为公共菜单=====================================--%>
<header>
 <div class="row">
  <div class="col-md-5">
   <a href="main.jsp"><img src="images/et_logo.png" alt="logo" style="margin-top: 6px" ></a>
  </div>
  <div class="col-md-4" style="margin-top:17px">
   <b class="text-muted" style="font-family: 仿宋;font-size: 25px;color: paleturquoise">___打造便捷快速的物流系统___</b>
  </div>
  <div class="col-md-3">
   <ul class="rt_nav">
    <li><a href="main.jsp" target="_self" class="website_icon">首页</a></li>
    <li><a href="login.html" class="quit_icon">安全退出</a></li>
   </ul>
  </div>
 </div>
</header>
<aside class="lt_aside_nav content mCustomScrollbar" style="margin-top:15px">
 <ul>
  <li>
   <dl>
    <dt>基础资料</dt>
    <!--当前链接则添加class:active-->
    <dd><a href="dept.jsp"  class="active">部门信息</a></dd>
    <dd><a href="emp.jsp">员工信息</a></dd>
    <dd><a href="car.jsp">车辆信息</a></dd>
    <dd><a href="client.jsp">客户信息</a></dd>
   </dl>
  </li>
  <li>
   <dl>
    <dt>货物托运</dt>
    <dd><a href="apply.jsp">货物托运订单</a></dd>
   </dl>
  </li>
  <li>
   <dl>
    <dt>车辆调度</dt>
    <dd><a href="adjust.jsp">调度信息</a></dd>
   </dl>
  </li>
 </ul>
</aside>


<%--================以上为公共菜单=====================================--%>


<%--数据表--%>
<section class="rt_wrap content mCustomScrollbar">
 <br>
 <div class="row">
  <div class="col-md-4">
   <lable for="searchName"></lable>
   <input type="text" id="searchName" placeholder="请输入部门名称搜索"/>
   <button type="button" class="btn btn-primary" onclick="findDept()">搜索</button>
  </div>
  <div class="col-md-offset-8">
   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addDept" style="margin-top: 20px">添加部门</button>
  </div>
 </div>
 <table class="table table-striped table-bordered table-hover">
  <caption>部门信息</caption>
  <thead>
  <tr>
   <th>部门序号</th>
   <th>部门名称</th>
   <th>最后一次更新时间</th>
   <th>操作</th>
  </tr>
  </thead>
  <tbody id="tb">
  </tbody>
 </table>
<%-- 分页标签--%>
 <ul class="pagination" style="margin-left: 35%" id="fy">
  <%--            id = "fy"--%>
 </ul>
</section>
<%--添加部门模态框 1--%>
<div class="modal fade" id="addDept" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" >添加部门</h4>
   </div>
   <div class="modal-body">
    <%--<div class="form-group">
    <label for="id">ID</label>
    <input type="text" class="form-control" id="id" placeholder="请输入ID">
</div>--%>
    <input type="hidden" class="form-control" id="id1" placeholder="请输入ID">
    <div class="form-group">
     <label for="name1">部门名称</label>
     <input type="text" class="form-control" id="name1" placeholder="请输入部门名称">
    </div>
   </div>
   <div class="modal-footer">
    <%--     点击按钮关闭模态框--%>
    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
    <%--     点击按钮提交更改--%>
    <button type="button" class="btn btn-primary" onclick="addDept()">添加</button>
   </div>
  </div><!-- /.modal-content -->
 </div><!-- /.modal -->
</div>
<%--修改部门模态框 2--%>
<div class="modal fade" id="alterDept" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" >部门编辑</h4>
   </div>
   <div class="modal-body">
    <%--<div class="form-group">
    <label for="id">ID</label>
    <input type="text" class="form-control" id="id" placeholder="请输入ID">
</div>--%>
    <input type="hidden" class="form-control" id="id2" placeholder="请输入ID">
    <div class="form-group">
     <label for="name2">部门名称</label>
     <input type="text" class="form-control" id="name2" placeholder="请输入部门名称">
    </div>
   </div>
   <div class="modal-footer">
                                                              <%--     点击按钮关闭模态框--%>
    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                                                               <%--     点击按钮提交更改--%>
    <button type="button" class="btn btn-primary" onclick="alterDept()">提交修改</button>
   </div>
  </div><!-- /.modal-content -->
 </div><!-- /.modal -->
</div>


</body>
<script src="js/upload.js"></script>
<script>
        var pageIndex = 1; //script初始化的时候，为第一页
        var pageCount = 5;//每页5个数据
       showDepts(pageIndex,pageCount); //一开始就调用该函数，把数据展示出来
        function showDepts(pageIndex,pageCount){//展示所有部门数据

         var searchName = $("#searchName").val();//获取输入框

              $.ajax({
               url:"/dept/getDepts",
               data:{
                 pageIndex:pageIndex,
                pageCount:pageCount,
                searchName:searchName,
               },
               type:"GET",
               contentType:"application/json",
               success:function (data){
                 console.log(data);
                 var html="";//用来渲染网页的html语句
                var count =data.number;
                data = data.data;
                data.forEach(function (d){ //遍历数据，添加到html语句渲染页面
                 html+=
                         '<tr>'+
                         '<td>' + d.id + '</td>' +
                         '<td>' + d.name + '</td>' +
                         '<td>' + d.updateTime+ '</td>' +
                         '<td>' +
                                                                                                         // 一起使用就是代表data-target所指的元素以data-toggle指定的形式显示
                         '<button type="button" class="btn btn-warning btn-xs"  data-toggle="modal" data-target="#alterDept" onclick="getDept(' + d.id + ')">修改</button>' +
                         '<button type="button" class="btn btn-danger btn-xs" style="margin-left:10px" onclick="deleteDept(' + d.id + ')">删除</button>' +
                         '</td>' +
                         '</tr>'
                })
                //主要是这一步，把数据放到页面上
                $("#tb").html(html);
                //渲染分页,通过jquery选择标签通过html方法渲染分页
                var fyhtml = '<li><a onclick="showDepts(' + (pageIndex - 1 > 0 ? pageIndex - 1 : 1) + ',' + pageCount + ')" href="#">&laquo;</a> </li>';
                //调用math.ceil()向上取整
                var page = Math.ceil(count / pageCount);
                for (var i = 1; i <= page; i++) {
                 fyhtml += '<li><a onclick="showDepts(' + i + ',' + pageCount + ')" href="#">' + i + '</a> </li>'
                }

                fyhtml += '<li><a onclick="showDepts(' + (pageIndex + 1 >= page ?  (pageIndex + 1) :page) + ',' + pageCount + ')" href="#">&raquo;</a> </li>';
                $("#fy").html(fyhtml);
               }

              })
        }

       function getDept(id){ //按照id获取当前部门信息，并显示到模态框上，方便修改部门信息
             $.ajax({
              url:"/dept/getDept",
              data:{
               id:id
              },
              type:"GET",
              contentType:"application/json",
              success:function (data){
               console.log(id)
               console.log(data);
               if (data.code == "1") {
                $("#id2").val(data.data.id);//在模态框中不显示，只是方便后续修改
                $("#name2").val(data.data.name);//这只是在模态框中显示的数据
              }else{
                alert("获取失败");
               }
              }
             })
       }

       function alterDept(){//按照Id修改部门数据
         var id = $("#id2").val();
         var name = $("#name2").val();

         if(!name){
          alert("部门名称未完善！");
          return;
         }

        $.ajax({
         url: "/dept/alterDept",//请求路径
         data: {
          id:id,
          name:name,
         },
         type: "GET",//请求方式
         contentType:"application/json",//返回数据类型
         success: function (data) {
          console.log(data);
          if (data.code == "1") {
           showDepts(1, 5);//刷新列表
           $("#alterDept").modal("hide");
           alert("修改成功");
          } else {
           alert("修改失败，"+data.msg);
          }
         }
        })
       }

       function deleteDept(id){//删除部门
          $.ajax({
           url:"/dept/deleteDept",
           data:{
            id:id,
           },
           type: "GET",//请求方式
           contentType:"application/json",//返回数据类型
           success:function (data){
            if (data.code == "1") {
             showDepts(1, 5);//刷新列表
             alert("删除成功");
            } else {
             alert("删除失败");
            }
           }
          })
       }

       function addDept(){
         var name  = $("#name1").val(); //获取信息
        if(!name){
         alert("部门名称未完善！");
         return;
        }
         $.ajax({
          url:"/dept/addDept",
          data:{
            name:name,
          },
          type: "GET",//请求方式
          contentType:"application/json",//返回数据类型
          success:function (data){
           if (data.code == "1") {
            showDepts(1, 5);//刷新列表
            $("#addDept").modal("hide");
            alert("添加成功")
           } else {
            alert("添加失败，"+data.msg);
           }
          }
         })
       }
       function findDept(){
         var searchName = $("#searchName").val();
         if(!searchName){
         alert("请输入部门名称！");
       }else
         {
          showDepts(1,5);
         }
       }
</script>
</html>
