<%--
  Created by IntelliJ IDEA.
  User: 25706
  Date: 2023/8/17
  Time: 20:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--加入jstl 标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>员工信息</title>
  <meta name="author" content="DeathGhost" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!--[if lt IE 9]>
  <script src="js/html5.js"></script>
  <![endif]-->
  <%--jsp = html+servlet--%>
  <%-- 注意顺序，先bootstrap.min.css
                     再到jquery
                     再到bootstrap.min.js
   否则某些组件不能正常显示--%>
  <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
  <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
  <script src="/js/jquery-3.3.1.js"></script>
  <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
  <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"
          integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
          crossorigin="anonymous"></script>
  <%-- <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">--%>
  <%-- <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

  <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="js/verificationNumbers.js"></script>
  <script src="js/Particleground.js"></script>
  <link rel="stylesheet" href="./css/fileinput.min.css">
  <script src="./js/fileinput.min.js"></script>
  <style>
    dd{
      font-size: 15px;
      font-family: 微軟正黑體;
      text-align: center;
    }
    dt{
      background-color:lightgreen;
      font-family: 黑体;
      font-size: 20px;
      text-align: center;
    }
    th{
      background-color: #c1e2b3;
      text-align: center;
      font-family: 楷体;
      font-size: 19px;
      /*font-weight: bold;*/
    }
    td{
      text-align: center;
      font-family: 等线;
      font-size: 15px;
    }
  </style>
</head>
<body>

<%--================以下为公共菜单=====================================--%>
<header>
  <div class="row">
    <div class="col-md-5">
      <a href="main.jsp"><img src="images/et_logo.png" alt="logo" style="margin-top: 6px" ></a>
    </div>
    <div class="col-md-4" style="margin-top:17px">
      <b class="text-muted" style="font-family: 仿宋;font-size: 25px;color: paleturquoise">___打造便捷快速的物流系统___</b>
    </div>
    <div class="col-md-3">
      <ul class="rt_nav">
        <li><a href="main.jsp" target="_self" class="website_icon">首页</a></li>
        <li><a href="login.html" class="quit_icon">安全退出</a></li>
      </ul>
    </div>
  </div>
</header>
<aside class="lt_aside_nav content mCustomScrollbar" style="margin-top:15px">
  <ul>
    <li>
      <dl>
        <dt>基础资料</dt>
        <!--当前链接则添加class:active-->
        <dd><a href="dept.jsp" >部门信息</a></dd>
        <dd><a href="emp.jsp" class="active">员工信息</a></dd>
        <dd><a href="car.jsp">车辆信息</a></dd>
        <dd><a href="client.jsp">客户信息</a></dd>
      </dl>
    </li>
    <li>
      <dl>
        <dt>货物托运</dt>
        <dd><a href="apply.jsp">货物托运订单</a></dd>
      </dl>
    </li>
    <li>
      <dl>
        <dt>车辆调度</dt>
        <dd><a href="adjust.jsp">调度信息</a></dd>
      </dl>
    </li>
  </ul>
</aside>


<%--================以上为公共菜单=====================================--%>


<%--数据表--%>
<section class="rt_wrap content mCustomScrollbar" >
  <br>
  <div class="row">
    <div class="col-md-4">
      <lable for="searchName"></lable>
      <input type="text" id="searchName" placeholder="请输入姓名搜索"/>
      <button type="button" class="btn btn-primary" onclick="findEmp()">搜索</button>
    </div>
    <div class="col-md-offset-8">
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addEmp" style="margin-top: 20px">添加员工</button>
    </div>
  </div>
  <table class="table table-striped table-bordered table-hover" >
    <caption>员工信息</caption>
    <thead>
    <tr>
      <th>工号</th>
      <th>姓名</th>
      <th>性别</th>
      <th>年龄</th>
      <th>职务</th>
      <th>所属部门</th>
      <th>最后一次更新时间</th>
      <th>操作</th>
    </tr>
    </thead>
    <tbody id="tb">
<%--    中间放表格数据--%>
    </tbody>
  </table>
  <%-- 分页标签--%>
  <ul class="pagination" style="margin-left: 35%" id="fy">
    <%--            id = "fy"--%>
  </ul>
</section>
<%--添加员工模态框 1--%>
<div class="modal fade" id="addEmp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >添加员工</h4>
      </div>
      <div class="modal-body">
        <%--<div class="form-group">
        <label for="id">ID</label>
        <input type="text" class="form-control" id="id" placeholder="请输入ID">
    </div>--%>
          <input type="hidden" class="form-control" id="id1" placeholder="请输入ID">
        <div class="form-group">
          <label for="name1">姓名</label>
          <input type="text" class="form-control" id="name1" placeholder="请输入姓名">
        </div>
          <div class="form-group" style="display: inline">
<%--            <label for="sex1">性别</label>
            <input type="text" class="form-control" id="sex1" placeholder="请输入性别">--%>
  <label>性别</label>
    <input type="radio" name="gender1" value="男">男</label>
    <input type="radio" name="gender1" value="女">女</label>
            <br><br>
          </div>
          <div class="form-group">
            <label for="age1">年龄</label>
            <input type="text" class="form-control" id="age1" placeholder="请输入年龄">
          </div>
          <div class="form-group">
            <label for="job1">职务</label>
            <input type="text" class="form-control" id="job1" placeholder="请输入职务">
          </div>
          <div class="form-group">
            <label for="dept1">所属部门</label>
            <input type="text" class="form-control" id="dept1" placeholder="请输入所属部门">
          </div>
      </div>
      <div class="modal-footer">
        <%--     点击按钮关闭模态框--%>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <%--     点击按钮提交更改--%>
        <button type="button" class="btn btn-primary" onclick="addEmp()">添加</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>
<%--修改员工信息模态框 2--%>
<div class="modal fade" id="alterEmp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >员工信息编辑</h4>
      </div>
      <div class="modal-body">
        <%--<div class="form-group">
        <label for="id">ID</label>
        <input type="text" class="form-control" id="id" placeholder="请输入ID">
    </div>--%>
          <input type="hidden" class="form-control" id="id2" placeholder="请输入ID">
          <div class="form-group">
            <label for="name2">姓名</label>
            <input type="text" class="form-control" id="name2" placeholder="请输入姓名">
          </div>
          <div class="form-group" style="display: inline">
            <%--            <label for="sex1">性别</label>
                        <input type="text" class="form-control" id="sex1" placeholder="请输入性别">--%>
            <label>性别</label>
            <input type="radio" name="gender2" value="男">男</label>
            <input type="radio" name="gender2" value="女">女</label>
            <br><br>
          </div>
          <div class="form-group">
            <label for="age2">年龄</label>
            <input type="text" class="form-control" id="age2" placeholder="请输入年龄">
          </div>
          <div class="form-group">
            <label for="job2">职务</label>
            <input type="text" class="form-control" id="job2" placeholder="请输入职务">
          </div>
          <div class="form-group">
            <label for="dept2">所属部门</label>
            <input type="text" class="form-control" id="dept2" placeholder="请输入所属部门">
          </div>
      </div>
      <div class="modal-footer">
        <%--     点击按钮关闭模态框--%>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <%--     点击按钮提交更改--%>
        <button type="button" class="btn btn-primary" onclick="alterEmp()">提交修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>
</body>
<script>
    var pageIndex = 1;
    var pageCount = 5;
    showEmps(pageIndex,pageCount);
    function showEmps(pageIndex,pageCount){//展示列表数据

      var searchName = $("#searchName").val();//获取搜索框的值,在此处可以顺便实现搜索功能

        $.ajax({
          url:"/emp/getEmps",
          data:{
            pageIndex:pageIndex,
            pageCount:pageCount,
            searchName:searchName,
          },
          type:"GET",
          contentType:"application/json",
          success:function (data){
            var html='';
            var count = data.number;
            data = data.data;
            data.forEach(function (e){
              html+='<tr>'+
                      '<td>' + e.id + '</td>' +
                      '<td>' + e.name + '</td>' +
                      '<td>' + e.sex + '</td>' +
                      '<td>' + e.age + '</td>' +
                      '<td>' + e.job + '</td>' +
                      '<td>' + e.deptName + '</td>' +
                      '<td>' + e.updateTime+ '</td>' +
                      '<td>' +
                      // 一起使用就是代表data-target所指的元素以data-toggle指定的形式显示
                      '<button type="button" class="btn btn-warning btn-xs"  data-toggle="modal" data-target="#alterEmp" onclick="getEmpById(' + e.id + ')">修改</button>' +
                      '<button type="button" class="btn btn-danger btn-xs" style="margin-left:10px" onclick="deleteEmp(' + e.id + ')">删除</button>' +
                      '</td>' +
                      '</tr>'
            })
            //主要是这一步，把数据放到页面上
            $("#tb").html(html);
            //userlist(1,5);
            console.log(html);
            //渲染分页,通过jquery选择标签通过html方法渲染分页
            var fyhtml = '<li><a onclick="showEmps(' + (pageIndex - 1 > 0 ? pageIndex - 1 : 1) + ',' + pageCount + ')" >&laquo;</a> </li>';
            //调用math.ceil()向上取整
            var page = Math.ceil(count / pageCount);
            for (var i = 1; i <= page; i++) {
              fyhtml += '<li><a onclick="showEmps(' + i + ',' + pageCount + ')" >' + i + '</a> </li>';
            }
            fyhtml += '<li><a onclick="showEmps(' + (pageIndex + 1 >= page ? (pageIndex + 1) :page) + ',' + pageCount + ')" >&raquo;</a> </li>';
            $("#fy").html(fyhtml);
          }
        })
    }

    function addEmp(){//添加员工

      var name = $("#name1").val();
      var sex = $('input[name="gender1"]:checked').val();
      var age = $("#age1").val();
      var job = $("#job1").val();
      var deptName = $("#dept1").val();

      if(!name || !sex || !age || !job ||!deptName){//其中一个有空，那就不能添加
            alert("员工信息未完善！");
            return;
      }
      $.ajax({
        url:"/emp/addEmp",
        data:JSON.stringify({id:1,name:name,sex:sex,age:age,job:job,deptName:deptName,updateTime:"0"}),
        type:"POST",
        datatype:"json",//请求数据类型
        contentType:"application/json",//返回数据类型
        success:function (data){
            if(data.code=="1"){
              showEmps(1,5);
              $("#addEmp").modal("hide");
              alert("添加成功");
            }
            else{
              alert("添加失败，不存在部门："+deptName);
              return;
            }
        }
      })
    }

    function getEmpById(id){//通过ID获取用户信息,方便修改
          $.ajax({
                url:"/emp/getEmpById",
                data:{
                  id:id,
                },
                type: "GET",
                contentType: "application/json",
                success:function(data){
                  if(data.code=="1"){
                    $("#id2").val(data.data.id);
                  $("#name2").val(data.data.name);
                  $("input[name='gender2']").attr("checked", true);
                  $("#age2").val(data.data.age);
                  $("#job2").val(data.data.job);
                  $("#dept2").val(data.data.deptName);
                  }
                  else{
                    alert("获取失败");
                  }
                }
          })
    }

    function alterEmp(){
     var id =  $("#id2").val();
      var name=$("#name2").val();
      var sex = $('input[name="gender2"]:checked').val();
     var age = $("#age2").val();
     var job = $("#job2").val();
     var deptName= $("#dept2").val();

      if(!name || !sex || !age ||!job ||!deptName){//其中一个有空，那就不能修改
        alert("员工信息未完善！");
        return;
      }

     $.ajax({
       url:"/emp/alterEmp",
       data:JSON.stringify({id:id,name:name,sex:sex,age:age,job:job,deptName:deptName,updateTime:"0"}),
       type:"POST",
       datatype: "json",
       contentType:"application/json",
       success:function (data){
         if(data.code=="1"){
           showEmps(1,5);
           $("#alterEmp").modal("hide");
           alert("修改成功");
         }
         else
         {
           alert("修改失败，不存在部门："+deptName);
         }
       }
     })
    }

    function deleteEmp(id){
      $.ajax({
        url:"/emp/deleteEmp",
        data:{
          id:id,
        },
        type:"GET",
        contentType:"application/json",
        success:function (data){
          if(data.code=="1"){
            showEmps(1,5);
            alert("删除成功");
          }else{
            alert("删除失败");
          }
        }
      })
    }

    function findEmp(){
      var searchName = $("#searchName").val();//获取搜索框
      if(!searchName){//搜索框为空
        alert("请输入姓名！");
        return;
      }
      else{//不为空
        showEmps(1,5);
      }
    }
</script>
</html>