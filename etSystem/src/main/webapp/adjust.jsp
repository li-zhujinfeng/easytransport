<%--
  Created by IntelliJ IDEA.
  User: 25706
  Date: 2023/8/17
  Time: 20:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--加入jstl 标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>调度信息</title>
  <meta name="author" content="DeathGhost" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!--[if lt IE 9]>
  <script src="js/html5.js"></script>
  <![endif]-->
  <%--jsp = html+servlet--%>
  <%-- 注意顺序，先bootstrap.min.css
                     再到jquery
                     再到bootstrap.min.js
   否则某些组件不能正常显示--%>

  <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
  <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
  <script src="<c:url value="/js/jquery-3.3.1.js"/>"></script>
<%--  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">--%>
  <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
  <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"
          integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
          crossorigin="anonymous"></script>
<%--  <link rel="stylesheet" href="css/bootstrap.min.css">--%>

<%--  <script src="js/bootstrap.js"></script>--%>
<%--  <script src="js/bootstrap.min.js"></script>--%>
  <link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
  <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
  <script src="js/bootstrap-datetimepicker.js"></script>
  <script src="js/bootstrap-datetimepicker.min.js"></script>
  <script src="js/bootstrap-datetimepicker.zh-CN.js"></script>
  <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="js/verificationNumbers.js"></script>
  <script src="js/Particleground.js"></script>
  <link rel="stylesheet" href="./css/fileinput.min.css">
  <script src="./js/fileinput.min.js"></script>
  <style>
    dd{
      font-size: 15px;
      font-family: 微軟正黑體;
      text-align: center;
    }
    dt{
      background-color:lightgreen;
      font-family: 黑体;
      font-size: 20px;
      text-align: center;
    }
    th{
      background-color: #c1e2b3;
      text-align: center;
      font-family: 楷体;
      font-size: 19px;
      /*font-weight: bold;*/
    }
    td{
      text-align: center;
      font-family: 等线;
      font-size: 15px;
    }
  </style>
</head>
<body>

<%--================以下为公共菜单=====================================--%>
<header>
  <div class="row">
    <div class="col-md-5">
      <a href="main.jsp"><img src="images/et_logo.png" alt="logo" style="margin-top: 6px" ></a>
    </div>
    <div class="col-md-4" style="margin-top:17px">
        <b class="text-muted" style="font-family: 仿宋;font-size: 25px;color: paleturquoise">___打造便捷快速的物流系统___</b>
    </div>
    <div class="col-md-3">
    <ul class="rt_nav">
      <li><a href="main.jsp" target="_self" class="website_icon">首页</a></li>
      <li><a href="login.html" class="quit_icon">安全退出</a></li>
    </ul>
    </div>
  </div>
</header>
<div class="row">
<aside class="lt_aside_nav content mCustomScrollbar" style="margin-top:15px">
  <ul>
    <li>
      <dl>
        <dt >基础资料</dt>
        <!--当前链接则添加class:active-->
        <dd><a href="dept.jsp" >部门信息</a></dd>
        <dd><a href="emp.jsp">员工信息</a></dd>
        <dd><a href="car.jsp">车辆信息</a></dd>
        <dd><a href="client.jsp">客户信息</a></dd>
      </dl>
    </li>
    <li>
      <dl>
        <dt style="background-color:lightgreen;font-family: 黑体;font-size: 20px" >货物托运</dt>
        <dd><a href="apply.jsp">货物托运订单</a></dd>
      </dl>
    </li>
    <li>
      <dl>
        <dt style="background-color:lightgreen;font-family: 黑体;font-size: 20px">车辆调度</dt>
        <dd><a href="adjust.jsp" class="active">调度信息</a></dd>
      </dl>
    </li>
  </ul>
</aside>
</div>
<%--================以上为公共菜单=====================================--%>
<%--数据表--%>
<section class="rt_wrap content mCustomScrollbar">
  <br>
  <div class="row">
    <div class="col-md-4">
      <lable for="searchId"></lable>
      <input type="text" id="searchId" placeholder="请输入调度号"/>
      <button type="button" class="btn btn-primary" onclick="findAdjust()">搜索</button>
    </div>
    <div class="col-md-offset-8">
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addAdjust" style="margin-top: 20px">添加调度</button>
    </div>
  </div>
  <table class="table table-striped table-bordered table-hover">
    <caption>调度</caption>
    <thead>
    <tr>
      <th>调度号(系统分配)</th>
      <th>司机工号</th>
      <th>驾驶车牌号</th>
      <th>始发地</th>
      <th>出发时间</th>
      <th>最后一次更新时间</th>
      <th>操作</th>
    </tr>
    </thead>
    <tbody id="tb">
    <%--    中间放表格数据--%>
    </tbody>
  </table>
  <%-- 分页标签--%>
  <ul class="pagination" style="margin-left: 35%" id="fy">
    <%--            id = "fy"--%>
  </ul>
</section>
<%--添加调度模态框 1--%>
<div class="modal fade" id="addAdjust" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >添加调度</h4>
      </div>
      <div class="modal-body">
        <%--<div class="form-group">
        <label for="id">ID</label>
        <input type="text" class="form-control" id="id" placeholder="请输入ID">
    </div>--%>
        <input type="hidden" class="form-control" id="id1" placeholder="请输入调度号">
        <div class="form-group">
          <label for="empId1">司机工号</label>
          <input type="text" class="form-control" id="empId1" placeholder="请输入司机工号">
        </div>
        <div class="form-group">
          <label for="carNumber1">驾驶车牌号</label>
          <input type="text" class="form-control" id="carNumber1" placeholder="请输入车牌号">
        </div>
        <div class="form-group">
          <label for="beginAddress1">始发地</label>
          <input type="text" class="form-control" id="beginAddress1" placeholder="请输入始发地">
        </div>
          <div class="form-group">
            <label for="beginTime1">出发时间选择</label>
            <div class='input-group date' >
              <input type='text' class="form-control" id='beginTime1'  placeholder="请选择时间"/>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <%--     点击按钮关闭模态框--%>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <%--     点击按钮提交更改--%>
        <button type="button" class="btn btn-primary" onclick="addAdjust()">添加</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>
<%--修改调度信息模态框 2--%>
<div class="modal fade" id="alterAdjust" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >调度信息编辑</h4>
      </div>
      <div class="modal-body">
        <%--<div class="form-group">
        <label for="id">ID</label>
        <input type="text" class="form-control" id="id" placeholder="请输入ID">
    </div>--%>
          <input type="hidden" class="form-control" id="id2" placeholder="请输入调度号">
          <div class="form-group">
            <label for="empId2">司机工号</label>
            <input type="text" class="form-control" id="empId2" placeholder="请输入司机工号">
          </div>
          <div class="form-group">
            <label for="carNumber2">驾驶车牌号</label>
            <input type="text" class="form-control" id="carNumber2" placeholder="请输入车牌号">
          </div>
          <div class="form-group">
            <label for="beginAddress2">始发地</label>
            <input type="text" class="form-control" id="beginAddress2" placeholder="请输入始发地">
          </div>
          <div class="form-group">
            <label for="beginTime2">出发时间选择</label>
            <div class='input-group date'>
              <input type='text' class="form-control"   id='beginTime2' placeholder="请选择时间"/>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <%--     点击按钮关闭模态框--%>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <%--     点击按钮提交更改--%>
        <button type="button" class="btn btn-primary" onclick="alterAdjust()">提交修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>

</body>
<script type="text/javascript">//用来修饰时间选择器的
//要分开写，因为是两个函数
$(function() {
  $('#beginTime1').datetimepicker()({
    format: 'yyyy-MM-dd HH:mm:ss',
    language: 'zh-CN',
    weekStart: 1,
    autoclose: true,
    todayHighlight: true
  });
});
$(function() {
  $('#beginTime2').datetimepicker()({
    format: 'yyyy-MM-dd HH:mm:ss',
    language: 'zh-CN',
    weekStart: 1,
    autoclose: true,
    todayHighlight: true
  });
});


</script>
<script>
  var pageIndex = 1;
  var pageCount = 5;
  showAdjusts(pageIndex,pageCount);
  function showAdjusts(pageIndex,pageCount){//展示列表数据

    var searchId = $("#searchId").val();//获取搜索框的值

    $.ajax({
      url:"/adjust/getAdjusts",
      data:{
        pageIndex:pageIndex,
        pageCount:pageCount,
        searchId:searchId,
      },
      type:"GET",
      contentType:"application/json",
      success:function (data){
        var html='';
        var count = data.number;
        data = data.data;
        data.forEach(function (a){
          html+='<tr>'+
                  '<td>' + a.id + '</td>' +
                  '<td>' + a.empId + '</td>' +
                  '<td>' + a.carNumber + '</td>' +
                  '<td>' + a.beginAddress + '</td>' +
                  '<td>' + a.beginTime + '</td>' +
                  '<td>' + a.updateTime+ '</td>' +
                  '<td>' +
                  // 一起使用就是代表data-target所指的元素以data-toggle指定的形式显示
                  '<button type="button" class="btn btn-warning btn-xs"  data-toggle="modal" data-target="#alterAdjust" onclick="getAdjustById(' + a.id + ')">修改</button>' +
                  '<button type="button" class="btn btn-danger btn-xs" style="margin-left:10px" onclick="deleteAdjust(' + a.id + ')">删除</button>' +
                  '</td>' +
                  '</tr>'
        })
        //主要是这一步，把数据放到页面上
        $("#tb").html(html);
        //userlist(1,5);
        console.log(html);
        //渲染分页,通过jquery选择标签通过html方法渲染分页
        var fyhtml = '<li><a onclick="showAdjusts(' + (pageIndex - 1 > 0 ? pageIndex - 1 : 1) + ',' + pageCount + ')" href="#">&laquo;</a> </li>';
        //调用math.ceil()向上取整
        var page = Math.ceil(count / pageCount);
        for (var i = 1; i <= page; i++) {
          fyhtml += '<li><a onclick="showAdjusts(' + i + ',' + pageCount + ')" href="#">' + i + '</a> </li>'
        }

        fyhtml += '<li><a onclick="showAdjusts(' + (pageIndex + 1 >= page ?  (pageIndex + 1) :page) + ',' + pageCount + ')" href="#">&raquo;</a> </li>';
        $("#fy").html(fyhtml);
      }
    })
  }

  function addAdjust(){//添加调度
    var empId = $("#empId1").val();
    var carNumber = $("#carNumber1").val();
    var beginAddress = $("#beginAddress1").val();
    var beginTime=$("#beginTime1").val();

  if(!empId || !carNumber||!beginTime||!beginAddress){
    alert("调度信息未完善！");
    return;
  }
    $.ajax({
      url:"/adjust/addAdjust",
      data:JSON.stringify({id:1,empId:empId,carNumber:carNumber,beginAddress:beginAddress,beginTime:beginTime,updateTime:"0"}),
      type:"POST",
      datatype:"json",//请求数据类型
      contentType:"application/json",//返回数据类型
      success:function (data){
        if(data.code=="1"){
          showAdjusts(1,5);
          $("#addAdjust").modal("hide");
          alert("添加成功");
        }
        else{
          alert("添加失败");
        }
      }
    })
  }

  function getAdjustById(id){//通过ID获取调度信息,方便修改
    $.ajax({
      url:"/adjust/getAdjustById",
      data:{
        id:id,
      },
      type: "GET",
      contentType: "application/json",
      success:function(data){
        if(data.code=="1"){
          $("#id2").val(data.data.id);
          $("#empId2").val(data.data.empId);
          $("#carNumber2").val(data.data.carNumber);
          $("#beginAddress2").val(data.data.beginAddress);
          $("#beginTime2").val(data.data.beginTime);
        }
        else{
          alert("获取失败");
        }
      }
    })
  }

  function alterAdjust(){
    var id =  $("#id2").val();
    var empId=$("#empId2").val();
    var carNumber= $("#carNumber2").val();
    var beginAddress = $("#beginAddress2").val();
    var beginTime=$("#beginTime2").val();

    if(!empId || !carNumber||!beginTime||!beginAddress){
      alert("调度信息未完善！");
      return;
    }

    $.ajax({
      url:"/adjust/alterAdjust",
      data:JSON.stringify({id:id,empId:empId,carNumber:carNumber,beginAddress:beginAddress,beginTime:beginTime,updateTime:"0"}),
      type:"POST",
      datatype: "json",
      contentType:"application/json",
      success:function (data){
        if(data.code=="1"){
          showAdjusts(1,5);
          $("#alterAdjust").modal("hide");
          alert("修改成功");
        }
        else
        {
          alert("修改失败");
        }
      }
    })
  }

  function deleteAdjust(id){
    $.ajax({
      url:"/adjust/deleteAdjust",
      data:{
        id:id,
      },
      type:"GET",
      contentType:"application/json",
      success:function (data){
        if(data.code=="1"){
          showAdjusts(1,5);
          alert("删除成功");
        }else{
          alert("删除失败");
        }
      }
    })
  }

  function findAdjust(){
      var searchId = $("#searchId").val();
      if(!searchId){
        alert("请输入调度号！");
        return;
      }else
      {
        showAdjusts(1,5);
      }
  }
</script>

</html>
