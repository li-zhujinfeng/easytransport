<%--
  Created by IntelliJ IDEA.
  User: 25706
  Date: 2023/8/17
  Time: 20:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--加入jstl 标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>货物托运申请</title>
  <meta name="author" content="DeathGhost" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!--[if lt IE 9]>
  <script src="js/html5.js"></script>
  <![endif]-->
  <%--jsp = html+servlet--%>
  <%-- 注意顺序，先bootstrap.min.css
                     再到jquery
                     再到bootstrap.min.js
   否则某些组件不能正常显示--%>
  <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
  <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
  <script src="/js/jquery-3.3.1.js"></script>
  <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
  <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"
          integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
          crossorigin="anonymous"></script>
  <%-- <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">--%>
  <%-- <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

  <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="js/verificationNumbers.js"></script>
  <script src="js/Particleground.js"></script>
  <link rel="stylesheet" href="./css/fileinput.min.css">
  <script src="./js/fileinput.min.js"></script>
  <style>
    dd{
      font-size: 15px;
      font-family: 微軟正黑體;
      text-align: center;
    }
    dt{
      background-color:lightgreen;
      font-family: 黑体;
      font-size: 20px;
      text-align: center;
    }
    th{
      background-color: #c1e2b3;
      text-align: center;
      font-family: 楷体;
      font-size: 19px;
      /*font-weight: bold;*/
    }
    td{
      text-align: center;
      font-family: 等线;
      font-size: 15px;
    }
  </style>
</head>
<body>

<%--================以下为公共菜单=====================================--%>
<header>
  <div class="row">
    <div class="col-md-5">
      <a href="main.jsp"><img src="images/et_logo.png" alt="logo" style="margin-top: 6px" ></a>
    </div>
    <div class="col-md-4" style="margin-top:17px">
      <b class="text-muted" style="font-family: 仿宋;font-size: 25px;color: paleturquoise">___打造便捷快速的物流系统___</b>
    </div>
    <div class="col-md-3">
      <ul class="rt_nav">
        <li><a href="main.jsp" target="_self" class="website_icon">首页</a></li>
        <li><a href="login.html" class="quit_icon">安全退出</a></li>
      </ul>
    </div>
  </div>
</header>
<aside class="lt_aside_nav content mCustomScrollbar" style="margin-top:15px">
  <ul>
    <li>
      <dl>
        <dt>基础资料</dt>
        <!--当前链接则添加class:active-->
        <dd><a href="dept.jsp">部门信息</a></dd>
        <dd><a href="emp.jsp">员工信息</a></dd>
        <dd><a href="car.jsp">车辆信息</a></dd>
        <dd><a href="client.jsp">客户信息</a></dd>
      </dl>
    </li>
    <li>
      <dl>
        <dt>货物托运</dt>
        <dd><a href="apply.jsp"  class="active">货物托运订单</a></dd>
      </dl>
    </li>
    <li>
      <dl>
        <dt>车辆调度</dt>
        <dd><a href="adjust.jsp">调度信息</a></dd>
      </dl>
    </li>
  </ul>
</aside>


<%--================以上为公共菜单=====================================--%>


<%--数据表--%>
<section class="rt_wrap content mCustomScrollbar">
  <br>
  <div class="row">
    <div class="col-md-4">
      <lable for="searchId"></lable>
      <input type="text" id="searchId" placeholder="请输入订单号搜索"/>
      <button type="button" class="btn btn-primary" onclick="findApply()">搜索</button>
    </div>
    <div class="col-md-offset-8">
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addApply" style="margin-top: 20px">添加订单</button>
    </div>
  </div>
  <table class="table table-striped table-bordered table-hover">
    <caption>货物托运订单</caption>
    <thead>
    <tr>
      <th>订单号</th>
      <th>托运调度号</th>
      <th>申请人姓名</th>
      <th>申请人手机号</th>
      <th>寄出地址</th>
      <th>收货人姓名</th>
      <th>收货人手机号</th>
      <th>收货人地址</th>
      <th>订单价格(元)</th>
      <th>最后一次更新时间</th>
      <th>操作</th>
    </tr>
    </thead>
    <tbody id="tb">
    <%--    中间放表格数据--%>
    </tbody>
  </table>
  <%-- 分页标签--%>
  <ul class="pagination" style="margin-left: 35%" id="fy">
    <%--            id = "fy"--%>
  </ul>
</section>
<%--添加订单模态框 1--%>
<div class="modal fade" id="addApply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >添加订单</h4>
      </div>
      <div class="modal-body">
        <%--<div class="form-group">
        <label for="id">ID</label>
        <input type="text" class="form-control" id="id" placeholder="请输入ID">
    </div>--%>
        <input type="hidden" class="form-control" id="id1" placeholder="请输入ID">
          <div class="form-group">
            <label for="adjustId1">调度号</label>
            <input type="text" class="form-control" id="adjustId1" placeholder="请在调度信息表中寻找已有调度号">
          </div>
        <div class="form-group">
          <label for="clientName1">申请人姓名</label>
          <input type="text" class="form-control" id="clientName1" placeholder="请输入申请人姓名">
        </div>
        <div class="form-group">
          <label for="clientPhone1">申请人手机号</label>
          <input type="text" class="form-control" id="clientPhone1" placeholder="请输入申请人手机号">
        </div>
        <div class="form-group">
          <label for="clientAddress1">寄出地址</label>
          <input type="text" class="form-control" id="clientAddress1" placeholder="请输入寄出地址">
        </div>
          <div class="form-group">
            <label for="receiveName1">收货人姓名</label>
            <input type="text" class="form-control" id="receiveName1" placeholder="请输入收货人姓名">
          </div>
          <div class="form-group">
            <label for="receivePhone1">收货人手机号</label>
            <input type="text" class="form-control" id="receivePhone1" placeholder="请输入收货人手机号">
          </div>
          <div class="form-group">
            <label for="receiveAddress1">收货人地址</label>
            <input type="text" class="form-control" id="receiveAddress1" placeholder="请输入收货人地址">
          </div>
      </div>
      <div class="modal-footer">
        <%--     点击按钮关闭模态框--%>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <%--     点击按钮提交更改--%>
        <button type="button" class="btn btn-primary" onclick="addApply()">添加</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>
<%--修改订单信息模态框 2--%>
<div class="modal fade" id="alterClient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >客户信息编辑</h4>
      </div>
      <div class="modal-body">
        <%--<div class="form-group">
        <label for="id">ID</label>
        <input type="text" class="form-control" id="id" placeholder="请输入ID">
    </div>--%>
        <input type="hidden" class="form-control" id="id2" placeholder="请输入ID">
          <div class="form-group">
            <label for="adjustId2">调度号</label>
            <input type="text" class="form-control" id="adjustId2" placeholder="请在调度信息表中寻找已有调度号">
          </div>
          <div class="form-group">
            <label for="clientName2">申请人姓名</label>
            <input type="text" class="form-control" id="clientName2" placeholder="请输入申请人姓名">
          </div>
          <div class="form-group">
            <label for="clientPhone2">申请人手机号</label>
            <input type="text" class="form-control" id="clientPhone2" placeholder="请输入申请人手机号">
          </div>
          <div class="form-group">
            <label for="clientAddress2">寄出地址</label>
            <input type="text" class="form-control" id="clientAddress2" placeholder="请输入寄出地址">
          </div>
          <div class="form-group">
            <label for="receiveName2">收货人姓名</label>
            <input type="text" class="form-control" id="receiveName2" placeholder="请输入收货人姓名">
          </div>
          <div class="form-group">
            <label for="receivePhone2">收货人手机号</label>
            <input type="text" class="form-control" id="receivePhone2" placeholder="请输入收货人手机号">
          </div>
          <div class="form-group">
            <label for="receiveAddress2">收货人地址</label>
            <input type="text" class="form-control" id="receiveAddress2" placeholder="请输入收货人地址">
          </div>
      </div>
      <div class="modal-footer">
        <%--     点击按钮关闭模态框--%>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <%--     点击按钮提交更改--%>
        <button type="button" class="btn btn-primary" onclick="alterApply()">提交修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>

</body>

<script>
  var pageIndex = 1;
  var pageCount = 5;
  showApplys(pageIndex,pageCount);
  function showApplys(pageIndex,pageCount){//展示列表数据

    var searchId = $("#searchId").val();//获取搜索框的值,在此处可以顺便实现搜索功能

    $.ajax({
      url:"/apply/getApplys",
      data:{
        pageIndex:pageIndex,
        pageCount:pageCount,
        searchId:searchId,
      },
      type:"GET",
      contentType:"application/json",
      success:function (data){
        var html='';
        var count = data.number;
        data = data.data;
        data.forEach(function (a){

          html+='<tr>'+
                  '<td>' + a.id + '</td>' +
                  '<td>' + a.adjustId + '</td>' +
                  '<td>' + a.clientName + '</td>' +
                  '<td>' + a.clientPhone + '</td>' +
                  '<td>' + a.clientAddress + '</td>' +
                  '<td>' + a.receiveName + '</td>' +
                  '<td>' + a.receivePhone+ '</td>' +
                  '<td>' + a.receiveAddress+ '</td>' +
                  '<td>' + a.price+ '</td>' +
                  '<td>' + a.updateTime+ '</td>' +
                  '<td>' +
                  // 一起使用就是代表data-target所指的元素以data-toggle指定的形式显示
                  '<button type="button" class="btn btn-warning btn-xs"  data-toggle="modal" data-target="#alterEmp" onclick="getApplyById(' + a.id + ')">修改</button>' +
                  '<button type="button" class="btn btn-danger btn-xs" style="margin-left:10px" onclick="deleteApply(' + a.id + ')">删除</button>' +
                  '</td>' +
                  '</tr>'
        })
        //主要是这一步，把数据放到页面上
        $("#tb").html(html);
        //userlist(1,5);
        console.log(html);
        //渲染分页,通过jquery选择标签通过html方法渲染分页
        var fyhtml = '<li><a onclick="showApplys(' + (pageIndex - 1 > 0 ? pageIndex - 1 : 1) + ',' + pageCount + ')" href="#">&laquo;</a> </li>';
        //调用math.ceil()向上取整
        var page = Math.ceil(count / pageCount);
        for (var i = 1; i <= page; i++) {
          fyhtml += '<li><a onclick="showApplys(' + i + ',' + pageCount + ')" href="#">' + i + '</a> </li>'
        }

        fyhtml += '<li><a onclick="showApplys(' + (pageIndex + 1 >= page ? (pageIndex + 1) :page) + ',' + pageCount + ')" href="#">&raquo;</a> </li>';
        $("#fy").html(fyhtml);
      }
    })
  }

  function addApply(){//添加员工

    var adjustId=$("#adjustId1").val();
    var clientName =$("#clientName1").val();
    var clientPhone = $("#clientPhone1").val();
    var clientAddress=$("#clientAddress1").val();
    var receiveName = $("#receiveName1").val();
    var receivePhone=$("#receivePhone1").val();
    var receiveAddress=$("#receiveAddress1").val();

    if(!adjustId || !clientName || !clientPhone || !clientAddress ||!receiveName||!receivePhone||!receiveAddress){//其中一个有空，那就不能添加
      alert("调度信息未完善！");
      return;
    }
    $.ajax({
      url:"/apply/addApply",
      data:JSON.stringify({id:1,adjustId:adjustId,clientName:clientName,clientPhone:clientPhone,clientAddress:clientAddress,receiveName:receiveName,receivePhone:receivePhone,receiveAddress:receiveAddress,applyTime:"0",price:0,updateTime:"0"}),
      type:"POST",
      datatype:"json",//请求数据类型
      contentType:"application/json",//返回数据类型
      success:function (data){
        if(data.code=="1"){
          showApplys(1,5);
          $("#addApply").modal("hide");
          alert("添加成功");
        }
        else{
          $("#addApply").modal("hide");
          alert("添加失败");
        }
      }
    })
  }

  function getApplyById(id){//通过ID获取用户信息,方便修改
    $.ajax({
      url:"/apply/getApplyById",
      data:{
        id:id,
      },
      type: "GET",
      contentType: "application/json",
      success:function(data){
        if(data.code=="1"){
          $("#adjustId2").val(data.data.adjustId);
          $("#clientName2").val(data.data.clientName);
          $("#clientPhone2").val(data.data.clientPhone);
         $("#clientAddress2").val(data.data.clientAddress);
           $("#receiveName2").val(data.data.receiveName);
          $("#receivePhone2").val(data.data.receivePhone);
          $("#receiveAddress2").val(data.data.receiveAddress);
        }
        else{
          alert("获取失败");
        }
      }
    })
  }

  function alterApply(){
    var id=$("#id2").val();
    var adjustId=$("#adjustId2").val();
    var clientName =$("#clientName2").val();
    var clientPhone = $("#clientPhone2").val();
    var clientAddress=$("#clientAddress2").val();
    var receiveName = $("#receiveName2").val();
    var receivePhone=$("#receivePhone2").val();
    var receiveAddress=$("#receiveAddress2").val();

    if(!adjustId || !clientName || !clientPhone || !clientAddress ||!receiveName||!receivePhone||!receiveAddress){//其中一个有空，那就不能添加
      alert("调度信息未完善！");
      return;
    }

    $.ajax({
      url:"/apply/alterApply",
      data:JSON.stringify({id:id,adjustId:adjustId,clientName:clientName,clientPhone:clientPhone,clientAddress:clientAddress,receiveName:receiveName,receivePhone:receivePhone,receiveAddress:receiveAddress,applyTime:"0",price:0,updateTime:"0"}),
      type:"POST",
      datatype: "json",
      contentType:"application/json",
      success:function (data){
        if(data.code=="1"){
          showApplys(1,5);
          $("#alterApply").modal("hide");
          alert("修改成功");
        }
        else
        {
          $("#alterApply").modal("hide");
          alert("修改失败");
        }
      }
    })
  }

  function deleteApply(id){
    $.ajax({
      url:"/apply/deleteApply",
      data:{
        id:id,
      },
      type:"GET",
      contentType:"application/json",
      success:function (data){
        if(data.code=="1"){
          showApplys(1,5);
          alert("删除成功");
        }else{
          alert("删除失败");
        }
      }
    })
  }

  function findApply(){
    var searchId = $("#searchId").val();//获取搜索框
    if(!searchId){//搜索框为空
      alert("请输入订单号！");
      return;
    }
    else{//不为空
      showApplys(1,5);
    }
  }
</script>
</html>
