package com.lizuwang.Fiter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SessionFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        String url = req.getRequestURI();
        if (url.contains("login") || url.contains("images") || url.contains("js")||url.contains("booking")||url.contains("getEmptyRooms")) {
            filterChain.doFilter(req, resp);
        } else {
            String username = (String) req.getSession().getAttribute("username");
            if (username != null && username != "") {
                filterChain.doFilter(req, resp);
            } else {
                resp.sendRedirect("/login.html");
            }
        }
    }

    @Override
    public void destroy() {

    }
}
