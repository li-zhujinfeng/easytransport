package com.lizuwang.service;

import com.lizuwang.pojo.Emp;

import java.util.Map;

public interface empService {
    public Map<String,Object> getEmps(Integer pageIndex, Integer pageCount,String searchName);
    public Map<String,Object> getEmpById(Integer id);
    public Map<String,Object> addEmp(Emp emp);
    public Map<String,Object> alterEmp(Emp emp);
    public Map<String,Object> deleteEmp(Integer id);
    public Map<String,Object> countEmp();
}

