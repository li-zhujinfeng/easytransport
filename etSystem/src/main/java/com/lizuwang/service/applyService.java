package com.lizuwang.service;

import com.lizuwang.pojo.Apply;

import java.util.Map;

public interface applyService {
    public Map<String,Object> getApplys(Integer pageIndex, Integer pageCount, String searchId);
    public Map<String,Object> getApplyById(Integer id);
    public Map<String,Object> addApply(Apply apply);
    public Map<String,Object> alterApply(Apply apply);
    public Map<String,Object> deleteApply(Integer id);
    public Map<String,Object> countApply();
}
