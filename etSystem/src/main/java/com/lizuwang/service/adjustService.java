package com.lizuwang.service;

import com.lizuwang.pojo.Adjust;

import java.util.Map;

public interface adjustService {
    //以map键值对形式返回数据，可以返回多个信息
    public abstract Map<String,Object> getAdjusts(Integer pageIndex, Integer pageCount,String searchId);
    public Map<String,Object> getAdjustById(Integer id);
    public Map<String,Object> addAdjust(Adjust adjust);
    public Map<String,Object> alterAdjust(Adjust adjust);
    public Map<String,Object> deleteAdjust(Integer id);
    public Map<String,Object> countAdjust();
}
