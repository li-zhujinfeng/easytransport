package com.lizuwang.service.impl;

import com.lizuwang.mapper.adjustMapper;
import com.lizuwang.mapper.carMapper;
import com.lizuwang.mapper.empMapper;
import com.lizuwang.pojo.Adjust;
import com.lizuwang.service.adjustService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//该注解专门用于service层的实现类，其内部嵌套了component注解，@Service方便专门用于该层
//对应的bean对象会在IOC容器中，默认命名为(实现类名称【首字母小写】)，也可以通过@Service的value属性指定bean名称
@Service
public class adjustservice implements adjustService {
    @Resource
    private adjustMapper adjustmapper;//获取数据访问层接口
    @Resource
    private carMapper carmapper;
    @Resource
    private empMapper empmapper;
    @Override
    public Map<String, Object> getAdjusts(Integer pageIndex, Integer pageCount,String searchId) {
        Map<String,Object> map = new HashMap<>();
        Integer count = adjustmapper.countAdjust();
        List<Adjust> adjusts = adjustmapper.getAdjusts((pageIndex-1)*pageCount, pageCount,searchId);
        map.put("data",adjusts);
        map.put("number",count);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> getAdjustById(Integer id) {
        Map<String,Object> map = new HashMap<>();
        Adjust adjust = adjustmapper.getAdjustById(id);
        map.put("data",adjust);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> addAdjust(Adjust adjust) {
        Map<String,Object> map = new HashMap<>();
    
        Integer flagCar = carmapper.existCar(null,adjust.getCarNumber(),"未分配");//查询是否存在车牌号并且未分配
        Integer flagEmp = empmapper.existEmp(adjust.getEmpId());//查询是否存在司机
    
        System.out.println(adjust.getBeginTime());
        
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        adjust.setUpdateTime(time);
        System.out.println(adjust);
        
        if(flagCar>0&&flagEmp>0)//说明车牌号正确,并且司机存在
        {
            adjustmapper.addAdjust(adjust);
            carmapper.alterStatus(adjust.getCarNumber(),"运行中");
            map.put("code","1");//全部正确
            map.put("car","1");
            map.put("emp","1");
        }
        else//但凡一个不正确
        {
            map.put("code","0");
            
            if(flagCar>0){
                map.put("car","1");
                map.put("emp","0");
            }else if(flagEmp>0){
                map.put("car","0");
                map.put("emp","1");
            }
            else{
                map.put("car","0");
                map.put("emp","0");
            }

        }
        return map;
    }
    
    @Override
    public Map<String, Object> alterAdjust(Adjust adjust) {
        Map<String,Object> map = new HashMap<>();
    
        Integer flagCar = carmapper.existCar(null,adjust.getCarNumber(),"未分配");//查询是否存在车牌号并且未分配
        Integer flagEmp = empmapper.existEmp(adjust.getEmpId());//查询是否存在司机
        System.out.println(adjust.getBeginTime());
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        adjust.setUpdateTime(time);
    
        if(flagCar>0&&flagEmp>0)//说明车牌号正确,并且司机存在
        {
            adjustmapper.alterAdjust(adjust);
            carmapper.alterStatus(adjust.getCarNumber(),"运行中");
            map.put("code","1");//全部正确
            map.put("car","1");
            map.put("emp","1");
        }
        else//但凡一个不正确
        {
            map.put("code","0");
        
            if(flagCar>0){
                map.put("car","1");
                map.put("emp","0");
            }else if(flagEmp>0){
                map.put("car","0");
                map.put("emp","1");
            }
            else{
                map.put("car","0");
                map.put("emp","0");
            }
        
        }
        return map;
    }
    
    @Override
    public Map<String, Object> deleteAdjust(Integer id) {
        Map<String,Object> map = new HashMap<>();
        Adjust adjustById = adjustmapper.getAdjustById(id);
        adjustmapper.deleteAdjust(id);
        carmapper.alterStatus(adjustById.getCarNumber(),"未分配");
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
    
    @Override
    public Map<String, Object> countAdjust() {
        Map<String,Object> map = new HashMap<>();
        Integer count= adjustmapper.countAdjust();
        map.put("number",count);
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
}
