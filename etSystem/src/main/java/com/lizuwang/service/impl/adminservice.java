package com.lizuwang.service.impl;
import com.lizuwang.mapper.adminMapper;
import com.lizuwang.pojo.Admin;
import com.lizuwang.service.adminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class adminservice implements adminService {
    @Resource
    private adminMapper adminmapper;
    
    @Override
    public Map<String, Object> getAdmin(Admin admin) {
        Map<String,Object> map = new HashMap<>();
        System.out.println(admin.getName()+admin.getPassword());
        if(adminmapper.getAdmin(admin)==null){
            map.put("code","0");
        }
        else{
            Admin data = adminmapper.getAdmin(admin);
            map.put("data",data);
            map.put("code","1");
        }
        return map;
    }
}
