package com.lizuwang.service.impl;

import com.lizuwang.mapper.adjustMapper;
import com.lizuwang.mapper.applyMapper;
import com.lizuwang.pojo.Apply;
import com.lizuwang.service.applyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "applyBean") //修改IOC容器中生成的bean名称
public class applyservice implements applyService {
    @Resource
    private applyMapper applymapper;
    @Resource
    private adjustMapper adjustmapper;
    @Override
    public Map<String, Object> getApplys(Integer pageIndex, Integer pageCount, String searchId) {
        Map<String,Object> map = new HashMap<>();
        Integer count = applymapper.countApply();
        List<Apply> applys = applymapper.getApplys((pageIndex-1)*pageCount, pageCount,searchId);
        map.put("data",applys);
        map.put("number",count);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> getApplyById(Integer id) {
        Map<String,Object> map = new HashMap<>();
        Apply apply = applymapper.getApplyById(id);
        map.put("data",apply);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> addApply(Apply apply) {
        Map<String,Object> map = new HashMap<>();
    
        Integer flag = adjustmapper.existId(apply.getId());//判断是否存在调度号
    
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String updateTime=sdf.format(day);
        apply.setUpdateTime(updateTime);
        apply.setPrice(5.00);
        if(flag>0){//说明存在调度号
            applymapper.addApply(apply);
            map.put("code","1");
        }else{
            map.put("code","0");
        }
        return map;
    }
    
    @Override
    public Map<String, Object> alterApply(Apply apply) {
        Map<String,Object> map = new HashMap<>();
        
        Integer flag = adjustmapper.existId(apply.getId());//判断是否存在调度号
        
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        apply.setUpdateTime(time);
        
        if(flag>0){
            applymapper.alterApply(apply);
            map.put("code","1");
        }else{
            map.put("code","0");
        }
        return map;
    }
    
    @Override
    public Map<String, Object> deleteApply(Integer id) {
        Map<String,Object> map = new HashMap<>();
        applymapper.deleteApply(id);
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
    
    @Override
    public Map<String, Object> countApply() {
        Map<String,Object> map = new HashMap<>();
        Integer count = applymapper.countApply();
        map.put("number",count);  //如果前面删除失败，该行代码处于无效状态
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
}
