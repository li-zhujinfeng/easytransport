package com.lizuwang.service.impl;
import com.lizuwang.mapper.clientMapper;
import com.lizuwang.pojo.Client;
import com.lizuwang.service.clientService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class clientservice implements clientService {
    @Resource
    private clientMapper clientmapper;
    
    @Override
    public Map<String, Object> getClients(Integer pageIndex, Integer pageCount,String searchName) {
        Map<String,Object> map = new HashMap<>();
        Integer count = clientmapper.countClient();
        List<Client> clients = clientmapper.getClients((pageIndex - 1) * pageCount, pageCount,searchName);

        map.put("data",clients);
        map.put("number",count);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> getClientById(Integer id) {
        Map<String,Object> map = new HashMap<>();
        Client client = clientmapper.getClientById(id);
        map.put("data",client);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> addClient(Client client) {
        Map<String,Object> map = new HashMap<>();
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        client.setUpdateTime(time);
        clientmapper.addClient(client);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> alterClient(Client client) {
        Map<String,Object> map = new HashMap<>();
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        client.setUpdateTime(time);
        clientmapper.alterClient(client);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> deleteClient(Integer id) {
        Map<String,Object> map = new HashMap<>();
        clientmapper.deleteClient(id);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> countClient() {
        Map<String,Object> map = new HashMap<>();
        Integer count = clientmapper.countClient();
        map.put("number",count);  //如果前面删除失败，该行代码处于无效状态
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
}
