package com.lizuwang.service.impl;

import com.lizuwang.mapper.adjustMapper;
import com.lizuwang.mapper.carMapper;
import com.lizuwang.pojo.Car;
import com.lizuwang.service.carService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class carservice implements carService {
    @Resource
    private carMapper carmapper;
    @Resource
    private adjustMapper adjustmapper;
    @Override
    public Map<String, Object> getCars(Integer pageIndex, Integer pageCount,String searchCarNumber) {
        Map<String,Object> map = new HashMap<>();
        Integer count  = carmapper.countCar();
        List<Car> cars = carmapper.getCars((pageIndex - 1) * pageCount,pageCount,searchCarNumber);

        map.put("data",cars);
        map.put("number",count);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> getCarById(Integer id) {
        Map<String,Object> map = new HashMap<>();
        Car car = carmapper.getCarById(id);
        map.put("data",car);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> addCar(Car car) {
        Map<String,Object> map = new HashMap<>();
        Integer flag = carmapper.existCar(null,car.getCarNumber(), null);//判断车牌号是否存在
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        car.setUpdateTime(time);
        if(flag>0){
            map.put("code","0");
            map.put("msg","车牌号已存在！");
        }
        else{
            carmapper.addCar(car);
            map.put("code","1");
        }
        return map;
    }
    
    @Override
    public Map<String, Object> alterCar(Car car) {//先把车辆的状态找出来
        Map<String,Object> map = new HashMap<>();
        
        Integer flag = carmapper.existCar(car.getId(),car.getCarNumber(), null);//判断车牌号是否存在
        
        Date day=new Date();
        Car carById = carmapper.getCarById(car.getId());
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        car.setUpdateTime(time);
        car.setStatus(carById.getStatus());//把原有状态放进去
        if(flag>0){//说明车牌重复，无法修改
            map.put("code","0");
            map.put("msg","车牌号已存在！");
        }
        else{
            carmapper.alterCar(car);
            map.put("code","1");
        }
        return map;
    }
    
    @Override
    public Map<String, Object> deleteCar(Integer id) {
        Map<String,Object> map = new HashMap<>();
        carmapper.deleteCar(id);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> countCar() {
        Map<String,Object> map = new HashMap<>();
        Integer count = carmapper.countCar();
        map.put("number",count);  //如果前面删除失败，该行代码处于无效状态
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
}
