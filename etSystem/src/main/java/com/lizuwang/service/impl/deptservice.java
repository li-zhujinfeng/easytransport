package com.lizuwang.service.impl;

import com.lizuwang.mapper.deptMapper;
import com.lizuwang.pojo.Dept;
import com.lizuwang.service.deptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class deptservice implements deptService{
    @Resource
    private deptMapper deptmapper;

    @Override
    public Map<String, Object> getDepts(Integer pageIndex, Integer pageCount,String searchName) {
        Map<String,Object> map=new HashMap<String,Object>();
        Integer count = deptmapper.countDept();//数据个数
        List<Dept> depts = deptmapper.getDepts((pageIndex-1)*pageCount,pageCount,searchName);
        System.out.println(depts);
        map.put("data",depts);
        map.put("number",count);
        map.put("code","1"); //判别码
        return map;
    }

    @Override
    public Map<String, Object> getDept(Integer id) {
        Map<String,Object> map=new HashMap<String,Object>();
        Dept dept = deptmapper.getDept(id);
        System.out.println(dept);
        map.put("data",dept);
        map.put("code","1"); //判别码
        return map;
    }

    @Override
    public Map<String,Object> alterDept(Integer id, String name) {
        Map<String,Object> map=new HashMap<String,Object>();
    
        Integer flag = deptmapper.existDept(id,name);//查询是否已经存在需要修改的部门
        //在这里传入更新时间
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        if(flag>0){
            map.put("code","0");
            map.put("msg","部门已存在！");
        }else{
            deptmapper.alterDept(id,name,time);
            map.put("code","1");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteDept(Integer id) {
        Map<String,Object> map=new HashMap<String,Object>();
        deptmapper.deleteDept(id);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> addDept(String name) {
        Map<String,Object> map=new HashMap<String,Object>();
        Integer flag = deptmapper.existDept(null,name);//查询是否存在该部门
        //在这里传入更新时间
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        if(flag>0){
            map.put("code","0");
            map.put("msg","部门已存在！");
        }else{
            deptmapper.addDept(name,time);
            map.put("code","1");
        }
        return map;
    }
    
    @Override
    public Map<String, Object> countDept() {
        Map<String,Object> map = new HashMap<>();
        Integer count = deptmapper.countDept();
        map.put("number",count);  //如果前面删除失败，该行代码处于无效状态
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
}
