package com.lizuwang.service.impl;

import com.lizuwang.mapper.empMapper;
import com.lizuwang.pojo.Emp;
import com.lizuwang.service.empService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
//真正进行事务逻辑处理的层面，实现类
public class empservice implements empService {
    @Resource
    private empMapper emptmapper;
    @Resource
    private com.lizuwang.mapper.deptMapper deptmapper;
    @Override
    public Map<String, Object> getEmps(Integer pageIndex, Integer pageCount,String searchName) {
        Map<String,Object> map = new HashMap<>();
        Integer count = emptmapper.countEmp();
        List<Emp> emps = emptmapper.getEmps((pageIndex-1)*pageCount, pageCount,searchName);
        map.put("data",emps);
        map.put("number",count);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> getEmpById(Integer id) {
        Map<String,Object> map = new HashMap<>();
        Emp emp = emptmapper.getEmpById(id);
        map.put("data",emp);
        map.put("code","1");
        return map;
    }
    
    @Override
    public Map<String, Object> addEmp(Emp emp) { //还要判断部门是否存在
        Map<String,Object> map = new HashMap<>();
        Integer flag = deptmapper.existDept(null,emp.getDeptName());//根据记录返回条数判断是否存在该部门
        System.out.println(flag);
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        emp.setUpdateTime(time);
        System.out.println(emp);
        
       if(flag>0){
           emptmapper.addEmp(emp);
           map.put("code","1");//说明可以插入
       }else{
           map.put("code","0");
           map.put("msg","部门不存在！");
       }
        return map;
    }
    
    @Override
    public Map<String, Object> alterEmp(Emp emp) {
        Map<String,Object> map = new HashMap<>();
    
        Integer flag = deptmapper.existDept(null,emp.getDeptName());//根据记录返回条数判断是否存在该部门
        Date day=new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(day);
        emp.setUpdateTime(time);
        if(flag>0){
            emptmapper.alterEmp(emp);
            map.put("code","1");//说明可以修改
        }else{
            map.put("code","0");
            map.put("msg","部门不存在！");
        }
        return map;
    }
    
    @Override
    public Map<String, Object> deleteEmp(Integer id) {
        Map<String,Object> map = new HashMap<>();
        emptmapper.deleteEmp(id);
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
    
    @Override
    public Map<String, Object> countEmp() {
        Map<String,Object> map = new HashMap<>();
        Integer count = emptmapper.countEmp();
        map.put("number",count);  //如果前面删除失败，该行代码处于无效状态
        map.put("code","1");  //如果前面删除失败，该行代码处于无效状态
        return map;
    }
    
    
}
