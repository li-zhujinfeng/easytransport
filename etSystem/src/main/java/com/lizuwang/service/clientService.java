package com.lizuwang.service;

import com.lizuwang.pojo.Client;

import java.util.Map;

public interface clientService {
    public Map<String,Object> getClients(Integer pageIndex, Integer pageCount,String searchName);
    public Map<String,Object> getClientById(Integer id);
    public Map<String,Object> addClient(Client client);
    public Map<String,Object> alterClient(Client client);
    public Map<String,Object> deleteClient(Integer id);
    public Map<String,Object> countClient();
}
