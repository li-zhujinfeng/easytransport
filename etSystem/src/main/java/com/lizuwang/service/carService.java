package com.lizuwang.service;

import com.lizuwang.pojo.Car;

import java.util.Map;

public interface carService {
    public Map<String,Object> getCars(Integer pageIndex, Integer pageCount,String searchCarNumber);
    public Map<String,Object> getCarById(Integer id);
    public Map<String,Object> addCar(Car car);
    public Map<String,Object> alterCar(Car car);
    public Map<String,Object> deleteCar(Integer id);
    public Map<String,Object> countCar();
}
