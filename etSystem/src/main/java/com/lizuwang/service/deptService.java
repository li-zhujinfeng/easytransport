package com.lizuwang.service;

import java.util.Map;

public interface deptService {
    public Map<String,Object> getDepts(Integer pageIndex,Integer pageCount,String searchName);
    public Map<String,Object> getDept(Integer id);
    public Map<String,Object> alterDept(Integer id,String name);
    public Map<String,Object> deleteDept(Integer id);
    public Map<String,Object> addDept(String name);
    public Map<String,Object> countDept();
}
