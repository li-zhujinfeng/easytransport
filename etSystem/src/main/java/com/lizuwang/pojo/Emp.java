package com.lizuwang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//员工信息

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Emp {
    private Integer id;//工号
    private String name;//姓名
    private String sex;//性别
    private String age;//年龄
    private String job;//职务
    private String deptName;//所属部门
    private String updateTime;//更新时间 ！！！！要用String接收时间！！！！！！！！
}
