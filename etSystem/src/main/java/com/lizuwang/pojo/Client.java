package com.lizuwang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//客户信息

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Client {
    private Integer id;//序号
    private String name;//姓名
    private String phone;//手机号
    private String address;//地址
    private String updateTime;//登记时间 ！！！！要用String接收时间！！！！！！！！
}
