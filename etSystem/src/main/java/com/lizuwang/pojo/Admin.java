package com.lizuwang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//系统管理员

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Admin {
    private Integer id;//管理员编号
    private String name;//用户名
    private String password;//密码
}
