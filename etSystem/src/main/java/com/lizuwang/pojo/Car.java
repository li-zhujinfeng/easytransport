package com.lizuwang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//车辆信息

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Car {
    private Integer id;//序号
    private String carNumber;//车牌号
    private String type;//运输类型  一般  特殊
    private String status;//运输状态 运行中  未分配
    private String updateTime;//更新时间 ！！！！要用String接收时间！！！！！！！！
}
