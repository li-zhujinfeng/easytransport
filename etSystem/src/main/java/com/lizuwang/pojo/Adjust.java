package com.lizuwang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//调度信息

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Adjust {  //思路：首先要添加调度，才能衍生订单，车辆状态
    private Integer id;//调度号
    private Integer empId;//司机工号
    private String carNumber;//驾驶车牌号
    private String beginAddress;//始发地
    private String beginTime;//出发时间 ！！！！要用String接收时间！！！！！！！！
    private String updateTime;//更新时间
}
