package com.lizuwang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//部门信息

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Dept {
	private Integer id;//部门序号(id)
	private String name;//部门名称
	private String updateTime;//创建时间 ！！！！要用String接收时间！！！！！！！！ 否则无法驼峰映射
}
