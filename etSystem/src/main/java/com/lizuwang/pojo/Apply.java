package com.lizuwang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//托运申请信息

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Apply {
    private Integer id;//订单号
    private Integer adjustId;//调度号
    private String clientName;//申请人姓名
    private String clientPhone;//申请人手机号
    private String clientAddress;//寄出地址
    private String receiveName;//收货人姓名
    private String receivePhone;//收货人手机号
    private String receiveAddress;//收货人地址
    private Double price;//订单价格
    private String updateTime;//最后一次更新时间，修改信息时间改变
}
