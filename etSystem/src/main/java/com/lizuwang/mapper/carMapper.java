package com.lizuwang.mapper;

import com.lizuwang.pojo.Car;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface carMapper {
    //此处传递参数，多个参数一定要用@Param
    public List<Car> getCars(@Param("pageIndex") Integer pageIndex , @Param("pageCount") Integer pageCount,@Param("searchCarNumber") String searchCarNumber); //
    public Car getCarById(Integer id);
    public void addCar(Car car);
    public void alterCar(Car car);
    public void deleteCar(Integer id);
    public Integer existCar(@Param("id") Integer id,@Param("carNumber") String carNumber,@Param("status") String status);
    public void alterStatus(@Param("carNumber") String carNumber,@Param("status") String status);
    public Integer countCar();
    
}
