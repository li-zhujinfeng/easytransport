package com.lizuwang.mapper;

import com.lizuwang.pojo.Client;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface clientMapper {
    //此处传递参数，多个参数一定要用@Param
    public List<Client> getClients(@Param("pageIndex") Integer pageIndex , @Param("pageCount") Integer pageCount,@Param("searchName") String searchName); //
    public Client getClientById(Integer id);
    public void addClient(Client client);
    public void alterClient(Client client);
    public void deleteClient(Integer id);
    public Integer countClient();
}
