package com.lizuwang.mapper;

import com.lizuwang.pojo.Emp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface empMapper {
    //此处传递参数，多个参数一定要用@Param
    public List<Emp> getEmps(@Param("pageIndex") Integer pageIndex , @Param("pageCount") Integer pageCount,@Param("searchName") String searchName); //
    public Emp getEmpById(Integer id);
    public void addEmp(Emp emp);
    public void alterEmp(Emp emp);
    public void deleteEmp(Integer id);
    public Integer existEmp(Integer id);
    public Integer countEmp();
}
