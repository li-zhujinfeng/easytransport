package com.lizuwang.mapper;
import com.lizuwang.pojo.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface deptMapper {
    //此处传递参数，多个参数一定要用@Param
    public List<Dept> getDepts(@Param("pageIndex") Integer pageIndex , @Param("pageCount") Integer pageCount,@Param("searchName") String searchName); //获取部门数据，不需要查询，直接获取

    public Dept getDept(Integer id);

    public void alterDept(@Param("id") Integer id,@Param("name") String name,@Param("time") String time);

    public void deleteDept(Integer id);
    
    public void addDept(@Param("name") String name,@Param("time") String time);
    
    public Integer existDept(@Param("id") Integer id,@Param("name") String name);
    
    public Integer countDept();
}
