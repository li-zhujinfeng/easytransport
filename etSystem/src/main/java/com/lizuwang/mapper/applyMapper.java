package com.lizuwang.mapper;

import com.lizuwang.pojo.Apply;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface applyMapper {
    public List<Apply> getApplys(@Param("pageIndex") Integer pageIndex , @Param("pageCount") Integer pageCount, @Param("id") String  id); //
    public Apply getApplyById(Integer id);
    public void addApply(Apply apply);
    public void alterApply(Apply apply);
    public void deleteApply(Integer id);
    public Integer countApply();
}
