package com.lizuwang.mapper;


import com.lizuwang.pojo.Adjust;
import org.apache.ibatis.annotations.Param;

import java.util.List;
public interface adjustMapper {
    public List<Adjust> getAdjusts(@Param("pageIndex") Integer pageIndex , @Param("pageCount") Integer pageCount,@Param("id") String id); //
    public Adjust getAdjustById(Integer id);
    public void addAdjust(Adjust adjust);
    public void alterAdjust(Adjust adjust);
    public void deleteAdjust(Integer id);
    public Integer existAdjust(String carNumber);
    public Integer existId(Integer id);
    public Integer countAdjust();
}
