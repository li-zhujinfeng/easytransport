package com.lizuwang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizuwang.pojo.Car;
import com.lizuwang.service.carService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

//处理url后缀，对应处理不同的事务
@Slf4j //添加该注解，简化日志定义记录对象，对象名为log
@Controller
@RequestMapping("/car")
public class carController {
    @Resource
    private carService carservice;
    
    @RequestMapping("/getCars")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getCars(Integer pageIndex, Integer pageCount,String searchCarNumber){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("获取全部车辆信息");
        return JSON.parseObject(JSON.toJSONString(carservice.getCars(pageIndex,pageCount,searchCarNumber))) ;
    }
    
    @RequestMapping("/getCarById")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getCarById(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("通过id获取车辆信息");
        return JSON.parseObject(JSON.toJSONString(carservice.getCarById(id))) ;
    }
    
    @RequestMapping("/addCar")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject addCar(@RequestBody Car car){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("添加车辆");
        return JSON.parseObject(JSON.toJSONString(carservice.addCar(car))) ;
    }
    
    @RequestMapping("/alterCar")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject alterCar(@RequestBody Car car){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("修改车辆信息");
        return JSON.parseObject(JSON.toJSONString(carservice.alterCar(car))) ;
    }
    
    @RequestMapping("/deleteCar")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject deleteCar(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("删除车辆");
        return JSON.parseObject(JSON.toJSONString(carservice.deleteCar(id))) ;
    }
    
    @RequestMapping("/countCar")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject countCar(){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("查询车辆数量");
        return JSON.parseObject(JSON.toJSONString(carservice.countCar())) ;
    }
}
