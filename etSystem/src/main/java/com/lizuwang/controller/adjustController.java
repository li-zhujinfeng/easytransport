package com.lizuwang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizuwang.pojo.Adjust;
import com.lizuwang.service.adjustService;
import com.lizuwang.service.impl.adjustservice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

//处理url后缀，对应处理不同的事务
@Slf4j //添加该注解，简化日志定义记录对象，对象名为log
@RestController
@RequestMapping("/adjust") //网址前缀
public class adjustController {
    @Resource(name = "adjustservice")
    private adjustService adjustservice;//声明业务层接口
    
    @RequestMapping("/getAdjusts")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getAdjusts(Integer pageIndex, Integer pageCount,String searchId){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("获取全部调度信息");
        return JSON.parseObject(JSON.toJSONString(adjustservice.getAdjusts(pageIndex,pageCount,searchId))) ;
    }
    
    @RequestMapping("/getAdjustById")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getAdjustById(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("通过Id查询调度信息");
        return JSON.parseObject(JSON.toJSONString(adjustservice.getAdjustById(id))) ;
    }
    
    @RequestMapping("/addAdjust")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject addAdjust(@RequestBody Adjust adjust){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("添加调度");
        return JSON.parseObject(JSON.toJSONString(adjustservice.addAdjust(adjust))) ;
    }
    
    @RequestMapping("/alterAdjust")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject alterAdjust(@RequestBody Adjust adjust){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("修改调度信息");
        return JSON.parseObject(JSON.toJSONString(adjustservice.alterAdjust(adjust))) ;
    }
    
    @RequestMapping("/deleteAdjust")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject deleteAdjust(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("删除调度");
        return JSON.parseObject(JSON.toJSONString(adjustservice.deleteAdjust(id))) ;
    }
    
    @RequestMapping("/countAdjust")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject countAdjust(){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("查询调度数量");
        return JSON.parseObject(JSON.toJSONString(adjustservice.countAdjust())) ;
    }
}
