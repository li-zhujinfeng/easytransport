package com.lizuwang.controller;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizuwang.pojo.Client;
import com.lizuwang.service.clientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

//处理url后缀，对应处理不同的事务
@Slf4j //添加该注解，简化日志定义记录对象，对象名为log
@Controller
@RequestMapping("/client")
public class clientController {
    @Resource
    private clientService clientservive;
    
    @RequestMapping("/getClients")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getClients(Integer pageIndex, Integer pageCount,String searchName){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("获取全部客户信息");
        return JSON.parseObject(JSON.toJSONString(clientservive.getClients(pageIndex,pageCount,searchName))) ;
    }
    @RequestMapping("/getClientById")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getClientById(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("通过Id获取客户信息");
        return JSON.parseObject(JSON.toJSONString(clientservive.getClientById(id))) ;
    }
    @RequestMapping("/addClient")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject addClient(@RequestBody Client client){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("添加客户");
        return JSON.parseObject(JSON.toJSONString(clientservive.addClient(client))) ;
    }
    @RequestMapping("/alterClient")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject alterClient(@RequestBody Client client){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("修改全部客户信息");
        return JSON.parseObject(JSON.toJSONString(clientservive.alterClient(client))) ;
    }
    
    @RequestMapping("/deleteClient")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject deleteClient(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("删除客户");
        return JSON.parseObject(JSON.toJSONString(clientservive.deleteClient(id))) ;
    }
    
    @RequestMapping("/countClient")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject countClient(){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("查询客户数量");
        return JSON.parseObject(JSON.toJSONString(clientservive.countClient())) ;
    }
}
