package com.lizuwang.controller;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizuwang.pojo.Emp;
import com.lizuwang.service.deptService;
import com.lizuwang.service.empService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

//处理url后缀，对应处理不同的事务
@Slf4j //添加该注解，简化日志定义记录对象，对象名为log
@Controller
@RequestMapping("/emp")
public class empController {
    @Resource
    private empService empservice;
    @Resource
    private deptService deptservice;
    @RequestMapping("/getEmps")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getEmps(Integer pageIndex, Integer pageCount,String searchName){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("获取全部员工信息");
        return JSON.parseObject(JSON.toJSONString(empservice.getEmps(pageIndex,pageCount,searchName))) ;
    }
    
    @RequestMapping("/getEmpById")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getEmpById(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("通过Id查询员工信息");
        return JSON.parseObject(JSON.toJSONString(empservice.getEmpById(id))) ;
    }
    
    @RequestMapping("/addEmp")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject addEmp(@RequestBody Emp emp){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("添加员工");
        return JSON.parseObject(JSON.toJSONString(empservice.addEmp(emp))) ;
    }
    
    @RequestMapping("/alterEmp")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject alterEmp(@RequestBody Emp emp){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("修改员工信息");
        return JSON.parseObject(JSON.toJSONString(empservice.alterEmp(emp))) ;
    }
    
    @RequestMapping("/deleteEmp")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject deleterEmp(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("删除员工");
        return JSON.parseObject(JSON.toJSONString(empservice.deleteEmp(id))) ;
    }
    @RequestMapping("/countEmp")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject countEmp(){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("查询员工数量");
        return JSON.parseObject(JSON.toJSONString(empservice.countEmp())) ;
    }
}
