package com.lizuwang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizuwang.pojo.Admin;
import com.lizuwang.service.adminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

//处理url后缀，对应处理不同的事务
@Slf4j //添加该注解，简化日志定义记录对象，对象名为log
@Controller
@RequestMapping("/admin")
public class adminController {

    @Autowired
    @Qualifier("adminservice") //指定注入类，如果单用@Autowired,但接口有两个实现类，不能正确注入对象，就会报错
    private adminService adminservice;

    @RequestMapping("/getAdmin")
    @ResponseBody                    //需要使用包装类，否则无法找到参数
    public JSONObject getAdmin(String name,String password){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("寻找用户信息");
        return JSON.parseObject(JSON.toJSONString(adminservice.getAdmin(new Admin(null,name,password)))) ;
    }
}
