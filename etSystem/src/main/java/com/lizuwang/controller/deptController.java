package com.lizuwang.controller;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizuwang.service.deptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;


//处理url后缀，对应处理不同的事务
@Slf4j //添加该注解，简化日志定义记录对象，对象名为log
@Controller
@RequestMapping("/dept")
public class deptController {
//
//	@RequestMapping("/main")
//	public
    @Resource
    private deptService deptservice;
    @RequestMapping("/getDepts")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getDepts(Integer pageIndex, Integer pageCount,String searchName){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("获取全部部门信息");
        return JSON.parseObject(JSON.toJSONString(deptservice.getDepts(pageIndex,pageCount,searchName))) ;
    }

    @RequestMapping("/getDept")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getDept(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("获取单个部门信息");
        return JSON.parseObject(JSON.toJSONString(deptservice.getDept(id))) ;
    }
    @RequestMapping("/alterDept")
    @ResponseBody                    //需要使用包装类，否则无法找到参数
    public JSONObject alterDept(Integer id,String name){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("修改部门信息");
        return JSON.parseObject(JSON.toJSONString(deptservice.alterDept(id,name))) ;
    }

    @RequestMapping("/deleteDept")
    @ResponseBody                    //需要使用包装类，否则无法找到参数
    public JSONObject deleteDept(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("删除部门信息");
        return JSON.parseObject(JSON.toJSONString(deptservice.deleteDept(id))) ;
    }
    
    @RequestMapping("/addDept")
    @ResponseBody                    //需要使用包装类，否则无法找到参数
    public JSONObject addDept(String name){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("添加部门信息");
        return JSON.parseObject(JSON.toJSONString(deptservice.addDept(name))) ;
    }
    
    @RequestMapping("/countDept")
    @ResponseBody                    //需要使用包装类，否则无法找到参数
    public JSONObject countDept(){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("查询部门数量");
        return JSON.parseObject(JSON.toJSONString(deptservice.countDept())) ;
    }
}
