package com.lizuwang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizuwang.pojo.Apply;
import com.lizuwang.service.applyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

//处理url后缀，对应处理不同的事务
@Slf4j //添加该注解，简化日志定义记录对象，对象名为log
@RestController
@RequestMapping("/apply")
public class applyController {
    @Resource(name = "applyBean") //指定需要注入的bean对象
    private applyService applyservice;
    
    @RequestMapping("/getApplys")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getApplys(Integer pageIndex, Integer pageCount, String searchId){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("获取全部订单信息");
        return JSON.parseObject(JSON.toJSONString(applyservice.getApplys(pageIndex,pageCount,searchId))) ;
    }
    
    @RequestMapping("/getApplyById")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject getApplyById(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("通过Id查询订单信息");
        return JSON.parseObject(JSON.toJSONString(applyservice.getApplyById(id))) ;
    }
    
    @RequestMapping("/addApply")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject addApply(@RequestBody Apply apply){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("添加订单");
        return JSON.parseObject(JSON.toJSONString(applyservice.addApply(apply))) ;
    }
    
    @RequestMapping("/alterApply")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject alterApply(@RequestBody Apply apply){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("修改订单信息");
        return JSON.parseObject(JSON.toJSONString(applyservice.alterApply(apply))) ;
    }
    
    @RequestMapping("/deleteApply")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject deleteApply(Integer id){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("删除订单");
        return JSON.parseObject(JSON.toJSONString(applyservice.deleteApply(id))) ;
    }
    
    @RequestMapping("/countApply")
    @ResponseBody                    //需要使用包装类，否则无法找到资源
    public JSONObject countApply(){
        //toJSONString 方法将对象的属性值转换为JSON键值对（key-value),并封装在大括号中
        log.info("查询订单数量");
        return JSON.parseObject(JSON.toJSONString(applyservice.countApply())) ;
    }
}
