<%--
  Created by IntelliJ IDEA.
  User: 25706
  Date: 2023/8/31
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="zh-CN">
<head>
    <title>主页</title>
    <meta name="author" content="DeathGhost" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <!--[if lt IE 9]>
    <script src="js/html5.js"></script>
    <![endif]-->
    <%--jsp = html+servlet--%>
    <%-- 注意顺序，先bootstrap.min.css
                       再到jquery
                       再到bootstrap.min.js
     否则某些组件不能正常显示--%>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <script src="/js/jquery-3.3.1.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"
            integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
            crossorigin="anonymous"></script>
    <%-- <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">--%>
    <%-- <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/verificationNumbers.js"></script>
    <script src="js/Particleground.js"></script>
    <link rel="stylesheet" href="./css/fileinput.min.css">
    <script src="./js/fileinput.min.js"></script>
    <style>
        dd{
            font-size: 15px;
            font-family: 微軟正黑體;
            text-align: center;
        }
        dt{
            background-color:lightgreen;
            font-family: 黑体;
            font-size: 20px;
            text-align: center;
        }
        th{
            background-color: #c1e2b3;
            text-align: center;
            font-family: 楷体;
            font-size: 19px;
            /*font-weight: bold;*/
        }
        td{
            text-align: center;
            font-family: 等线;
            font-size: 15px;
        }
        h4{
            font-family: 黑体;
            font-size: 25px;
        }
        p{
            margin-left:200px;
        }
        b{
            color: red;
            font-family: 微软雅黑;
            font-size: 20px;
        }
    </style>
</head>
<body>

<%--================以下为公共菜单=====================================--%>
<header>
    <div class="row">
        <div class="col-md-5">
            <a href="main.jsp"><img src="images/et_logo.png" alt="logo" style="margin-top: 6px" ></a>
        </div>
        <div class="col-md-4" style="margin-top:17px">
            <b class="text-muted" style="font-family: 仿宋;font-size: 25px;color: paleturquoise">___打造便捷快速的物流系统___</b>
        </div>
        <div class="col-md-3">
            <ul class="rt_nav">
                <li><a href="main.jsp" target="_self" class="website_icon">首页</a></li>
                <li><a href="login.html" class="quit_icon">安全退出</a></li>
            </ul>
        </div>
    </div>
</header>
<aside class="lt_aside_nav content mCustomScrollbar" style="margin-top:15px">
    <ul>
        <li>
            <dl>
                <dt >基础资料</dt>
                <!--当前链接则添加class:active-->
                <dd><a href="dept.jsp" >部门信息</a></dd>
                <dd><a href="emp.jsp">员工信息</a></dd>
                <dd><a href="car.jsp">车辆信息</a></dd>
                <dd><a href="client.jsp">客户信息</a></dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt style="background-color:lightgreen;font-family: 黑体;font-size: 20px" >货物托运</dt>
                <dd><a href="apply.jsp">货物托运订单</a></dd>
            </dl>
        </li>
        <li>
            <dl>
                <dt style="background-color:lightgreen;font-family: 黑体;font-size: 20px">车辆调度</dt>
                <dd><a href="adjust.jsp" class="active">调度信息</a></dd>
            </dl>
        </li>
    </ul>
</aside>
<%--================以上为公共菜单=====================================--%>
<section class="rt_wrap content mCustomScrollbar" >
    <div class="row">
        <div class="col-md-6">
            <h1 style="color: #eea236"> 系统报表 </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="images/dept.png" alt="部门图片" style="width: 150px;height: 100px;">
                <div class="caption">
                    <h4 id="deptNumber" >部门数：</h4>
                    <p><a href="dept.jsp" class="btn btn-primary" role="button" >查看部门信息</a></p>
                </div>
            </div>
        </div>
        <div class=" col-md-4">
            <div class="thumbnail">
                <img src="images/emp.png" alt="员工图片" style="width: 150px;height: 100px;">
                <div class="caption">
                    <h4 id="empNumber">员工数：</h4>
                    <p><a href="emp.jsp" class="btn btn-primary" role="button">查看员工信息</a></p>
                </div>
            </div>
        </div>
        <div class=" col-md-4">
            <div class="thumbnail">
                <img src="images/car.png" alt="车辆图片" style="width: 150px;height: 100px;">
                <div class="caption">
                    <h4 id="carNumber">车辆数：</h4>
                    <p><a href="car.jsp" class="btn btn-primary" role="button">查看车辆信息</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="images/client.png" alt="客户图片" style="width: 150px;height: 100px;">
                <div class="caption">
                    <h4 id="clientNumber">客户数：</h4>
                    <p><a href="client.jsp" class="btn btn-primary" role="button">查看客户信息</a></p>
                </div>
            </div>
        </div>
        <div class=" col-md-4">
            <div class="thumbnail">
                <img src="images/apply.png" alt="订单图片" style="width: 150px;height: 100px;">
                <div class="caption">
                    <h4 id="applyNumber">订单数：</h4>
                    <p><a href="apply.jsp" class="btn btn-primary" role="button">查看订单信息</a></p>
                </div>
            </div>
        </div>
        <div class=" col-md-4">
            <div class="thumbnail">
                <img src="images/adjust.png" alt="调度图片" style="width: 150px;height: 100px;">
                <div class="caption">
                    <h4 id="adjustNumber">调度数：</h4>
                    <p><a href="adjust.jsp" class="btn btn-primary" role="button">查看调度信息</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
<script>
    countDept();
    countEmp();
    countCar();
    countClient();
    countApply();
    countAdjust();
    function countDept(){
        var html = '';
        $.ajax({
            url:"dept/countDept",
            type:"GET",
            contentType:"application/json",
            success:function (data){
                html+=
                    '<b style="color: red">' + data.number + '</b>';
                $("#deptNumber").append(html);
            }
        })
    }

    function countEmp(){
        var html = '';
        $.ajax({
            url:"emp/countEmp",
            type:"GET",
            contentType:"application/json",
            success:function (data){
                html+=
                    '<b style="color: red">' + data.number + '</b>';
                $("#empNumber").append(html);
            }
        })
    }

    function countCar(){
        var html = '';
        $.ajax({
            url:"car/countCar",
            type:"GET",
            contentType:"application/json",
            success:function (data){
                html+=
                    '<b style="color: red">' + data.number + '</b>';
                $("#carNumber").append(html);
            }
        })
    }

    function countClient(){
        var html = '';
        $.ajax({
            url:"client/countClient",
            type:"GET",
            contentType:"application/json",
            success:function (data){
                html+=
                    '<b style="color: red">' + data.number + '</b>';
                $("#clientNumber").append(html);
            }
        })
    }

    function countApply(){
        var html = '';
        $.ajax({
            url:"apply/countApply",
            type:"GET",
            contentType:"application/json",
            success:function (data){
                html+=
                    '<b style="color: red">' + data.number + '</b>';
                $("#applyNumber").append(html);
            }
        })
    }

    function countAdjust(){
        var html = '';
        $.ajax({
            url:"adjust/countAdjust",
            type:"GET",
            contentType:"application/json",
            success:function (data){
                html+=
                    '<b>' + data.number + '</b>';
                $("#adjustNumber").append(html);
            }
        })
    }
</script>
</html>
