# easytransport

#### 介绍
捷流系统，一个简洁的物流管理系统

#### 软件架构

基于SSM框架构建，使用本地tomcat服务器运行，数据库底层采用mysql数据库，druid连接池

#### 功能展示
1) 登录页面（包含动态粒子特效）
![输入图片说明](.idea/login1.png)

2)主页面
![输入图片说明](.idea/%E6%8A%A5%E8%A1%A8%E4%B8%BB%E7%95%8C%E9%9D%A2.png)

3）部分系统页面
![输入图片说明](.idea/libraries/%E8%BD%A6%E8%BE%86.png)

![输入图片说明](.idea/libraries/%E9%83%A8%E9%97%A8%E4%BF%A1%E6%81%AF.png)

![输入图片说明](.idea/libraries/%E6%B7%BB%E5%8A%A0%E8%AE%A2%E5%8D%95.png)

#### 特技


1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
